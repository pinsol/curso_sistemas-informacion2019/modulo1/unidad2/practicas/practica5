﻿                                            /** MODULO I - UNIDAD II - PRACTICA 5 **/

-- creando la base de datos
DROP DATABASE IF EXISTS practica5;
CREATE DATABASE IF NOT EXISTS practica5; 
USE practica5;

-- creando las tablas
CREATE OR REPLACE TABLE federacion(
  nombre varchar(15),
  direccion varchar(50),
  telefono varchar(12),

  PRIMARY KEY (nombre)
);

CREATE OR REPLACE TABLE miembro(
  dni varchar(10),
  nombre_m varchar(15),
  titulacion varchar(50),

  PRIMARY KEY (dni)
);

CREATE OR REPLACE TABLE composicion(
  nombre varchar(15),
  dni varchar(10),
  cargo varchar(30),
  fecha_inicio date,

  PRIMARY KEY (nombre,dni)
);

-- creando claves ajenas
ALTER TABLE composicion
ADD CONSTRAINT fkcomposicionfederacion
FOREIGN KEY (nombre) REFERENCES federacion(nombre);

ALTER TABLE composicion
ADD CONSTRAINT fkcomposicionmiembro
FOREIGN KEY (dni) REFERENCES miembro(dni);

-- introduciendo datos
INSERT INTO federacion (nombre, direccion, telefono)
  VALUES ('fede1', 'dir1', 'tlf1'),
         ('fede2', 'dir2', 'tlf2');

INSERT INTO miembro (dni, nombre_m, titulacion)
  VALUES ('1', 'pepe', 'titu1'),
         ('2', 'maria', 'titu2'),
         ('3', 'paco', 'titu3'),
         ('4', 'cintia', 'titu4');

INSERT INTO composicion (nombre, dni, cargo, fecha_inicio)
  VALUES ('fede1', '1', 'PRESIDENTE', CURDATE()),
         ('fede1', '2', 'GERENTE', CURDATE()),
         ('fede1', '3', 'ASESOR TECNICO', CURDATE()),
         ('fede1', '4', 'PSICOLOGO', CURDATE());

                                                           /* CONSULTAS */
-- 1.-
SELECT
  m.nombre_m
FROM
  composicion c
JOIN
  miembro m USING (dni)
WHERE
  c.cargo = 'PRESIDENTE'
;

-- optimizando la consulta
SELECT c1.nombre_m FROM
  (
    SELECT m.dni, m.nombre_m FROM miembro m
  ) c1
JOIN
  (
    SELECT c.dni FROM composicion c WHERE c.cargo = 'PRESIDENTE'
  ) c2
USING(dni)
;

-- 2.-
SELECT
  f.direccion
FROM
  composicion c
JOIN
  federacion f USING (nombre)
WHERE
  c.cargo = 'GERENTE'
;

-- optimizando consulta
SELECT c1.direccion FROM
  (
    SELECT f.nombre, f.direccion FROM federacion f
  ) c1
JOIN
  (
    SELECT c.nombre FROM composicion c WHERE c.cargo = 'GERENTE'
  ) c2
USING(nombre)
;

-- 3.-
SELECT
  f.nombre
FROM
  federacion f
LEFT JOIN
  (
    SELECT
      c.nombre
    FROM
      composicion c
    WHERE
      cargo = 'ASESOR TECNICO'
   ) c1 USING (nombre)
;

-- 4.-


-- 5.-
SELECT
  c1.nombre
FROM
  (
    SELECT
      c.nombre
    FROM
      composicion c
    WHERE
      cargo = 'ASESOR TECNICO'
  ) c1
 JOIN
  (
    SELECT
      c.nombre
    FROM
      composicion c
    WHERE
      c.cargo = 'PSICOLOGO'
  ) c2 USING (nombre)
;